---
id: devdocs_troubleshooting
layout: guide
---

#### ENOSPC: no space left on device <a class="toc" id="toc-enospc" href="#toc-enospc"></a>

Depending on your OS, you may face the following error :

```sh
# [nodemon] Internal watch failed: ENOSPC: no space left on device, watch '/PeerTube/dist'
```

This is due to your system's limit on the number of files you can monitor for live-checking changes. For example, Ubuntu uses inotify and this limit is set to 8192. Then you need to change this limit :

```sh
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

See more information [here](https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers).


#### "PeerTube is not compatible with your web browser..." <a class="toc" id="toc-notcompatible" href="#toc-notcompatible"></a>

When trying to run an ```npm run dev ``` and look your dev instance on your web browser, you may encounter a pop-up windows saying something like "PeerTube is not compatible with your web browser...". This message seems to indicate that your dependencies need to be updated. A simple way to fix this is to make sure to be up to date with your local repository and to run :
```sh 
yarn install --pure-lockfile 
```

You may be tempted to update your dependencies with npm instead of yarn, with an ```npm i ```. It would be a very bad idea. If ever you did it, try to delete ```package-lock.json``` and ```node_modules``` :

```sh
rm package-lock.json && rm -rf node_modules
```

Then, a simple ```yarn install --pure-lockfile``` should fix this.