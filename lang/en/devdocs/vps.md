---
id: devdocs_vps
layout: guide
---

### Setup a development environment on a VPS

If you want to develop using a Virtual Private Server, you will need to configure the url for the API and the hostname. First, you need to edit the [client/src/environments/environment.hmr.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/client/src/environments/environment.hmr.ts) file by replacing the `localhost` in the `apiUrl` field with the address of your VPS. Thus, the [Hot Module Replacement](https://webpack.js.org/concepts/hot-module-replacement/) from Webpack will be set up for developping with live-reload.

Next, you will need to edit the [config/default.yaml](https://github.com/Chocobozzz/PeerTube/blob/develop/config/default.yaml) file. Just replace the `localhost` with your VPS address in the following `hostname` fields :

```yaml
listen:
  hostname: 'my-vps-address.net'
  port: 9000

webserver:
  https: false
  hostname: 'my-vps-address.net'
  port: 9000
```

Then, you just need to listen to `https://my-vps-address.net:3000/` in your web browser.
