---
id: translations
guide: peertube_organization
layout: guide
timer: false
---

Documentation is written in English through the repository on GitHub. Documentation
on [this repository](https://framagit.org/framasoft/peertube/documentation) is also a translation target. Please feel free to fork it any way
you want to contribute back translations.

PeerTube itself needs translators. Please refer to [the translation guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/translation.md).

Remember that the [code of conduct]({{url_base}}/org/code-of-conduct.html) also
applies to translations. Try to make them neutral and void of RMS jokes.
