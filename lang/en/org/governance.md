---
id: governance
guide: peertube_organization
layout: guide
timer: false
---

Right now Chocobozzz manages most of the software life and incoming contributions. Members of the
Framasoft team also actively support PeerTube (infrastructure for the development tools, PR, financing, etc.).
No formal structure is yet in place to decide of roadmaps.
