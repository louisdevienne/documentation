<div class="install-only-beta" markdown="1">
On Arch Linux, PeerTube can be installed through the Arch User Repository thanks to a __community package__ made by [daftaupe](https://aur.archlinux.org/packages/peertube/).
 
```sh
asp checkout peertube
cd peertube
makepkg --syncdeps --rmdeps --install --clean
```

Or

```sh
yay -S peertube
```

</div>

#### Configuration

You now have to [configure the database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database)
and credentials to it in the configuration file of PeerTube
in `/usr/share/webapps/peertube/config/production.yaml`.

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Arch packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>
