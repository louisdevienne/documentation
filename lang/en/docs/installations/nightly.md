There are no nightly builds of PeerTube at the moment. Please use the development branch on the repository instead.

<!--
{{i18n.install_nightly_intro}}

The easiest way of installing a nightly build is via our shell script:

```sh
curl -o- -L https://pkg.rigelk.eu/install.sh | bash -s -- --nightly
```
-->
