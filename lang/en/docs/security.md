---
id: docs_security
guide: docs_security
layout: guide
---

Installing PeerTube following the production guide should be secure enough by default. We list here suggestions
to tighten the security of some parts of PeerTube.

## Run ffmpeg with reduced privileges <a class="toc" id="toc-run-ffmpeg-with-reduced-privileges" href="#toc-run-ffmpeg-with-reduced-privileges"></a>

<div class="alert alert-warning" role="alert">
  <strong>Warning:</strong> this section is experimental and subject to change.
  Don't copy commands without adapting them to your situation first.
</div>

`ffmpeg` is a project with a [lot of security fixes](https://www.ffmpeg.org/security.html) shipped every
version. Adding firejail, other than adding SELinux or AppArmor support, is a fast and portable way
across all Linux 3.x+ targets to provide some protection from malicious video files uploaded on your instance.
Firejail is a Linux SUID (Set owner User ID up on execution) sandbox with zero dependencies and lots of predefined profiles for common
programs, including ffmpeg. One should always be careful feeding ffmpeg with user-data. Sandboxing
it to, for instance, have no network access, can remove some attack vectors.

Install firejail and firejail-profiles:

```bash
apt install --no-install-recommends firejail firejail-profiles
```

Create the file `/usr/local/bin/jail_ffmpeg`:

```bash
#!/bin/bash
/usr/bin/firejail --writable-var /usr/bin/ffmpeg "$@"
```

Make sure it's executable with `chmod +x /usr/local/bin/jail_ffmpeg`. Repeat for `ffprobe`.

Once your firejail/other wrappers for `ffmpeg` and `ffprobe` are ready, you can make PeerTube use them
via environment variables for the process (here in a systemd unit):

```
Environment=FFMPEG_PATH=/usr/local/bin/jail_ffmpeg
Environment=FFPROBE_PATH=/usr/local/bin/jail_ffprobe
```

Now, `firejail` doesn't support home directories outside of `/home` [yet](https://github.com/netblue30/firejail/issues/2259#issuecomment-438355805), so we need
to relink PeerTube to its own home directory:

- create a symbolic link in `/home` pointing to the user's home directory `/var/www/peertube`
- in `/etc/passwd`, provide this symbolic link as home directory of user peertube

## Systemd Unit with reduced privileges <a class="toc" id="toc-systemd-unit-with-reduced-privileges" href="#toc-systemd-unit-with-reduced-privileges"></a>

A systemd unit template is provided at `support/systemd/peertube.service`.

### `PrivateDevices`

<div class="alert alert-warning" role="alert">
  <strong>Warning:</strong> this won't work on Raspberry Pi. That's
  why we don't enable it by default.
</div>

`PrivateDevices=true` sets up a new `/dev` mount for the Peertube process and
only adds API pseudo devices like `/dev/null`, `/dev/zero`, or `/dev/random`
but not physical devices.

### `ProtectHome`

`ProtectHome=true` sandboxes Peertube such that the service can not access the
`/home`, `/root`, and `/run/user` folders. If your local Peertube user has its
home folder in one of the restricted places, either change the home directory
of the user or set this option to `false`.
