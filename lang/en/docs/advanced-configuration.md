---
id: docs_advanced_configuration
guide: docs_advanced_configuration
layout: guide
timer: false
---

This is still a work in progress. Please refer to the configuration instructions
within the [PeerTube repository](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#peertube-configuration).
