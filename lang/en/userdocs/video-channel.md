---
id: userdocs_video_channel
guide: userdocs_video_channel
layout: userguide
additional_reading_tags: ["profile", "sharing"]
---

You might have noticed it: your account on a PeerTube instance has the notion of "channels". On the contrary to YouTube where an account corresponds to a channel, on PeerTube you can group your videos per theme ; those interested by your cat videos will then be able to subscribe to their dedicated channel, whereas those interested in dog videos won't see cats by just subscribing to the channel where you publish your dog videos.

## See and create channels <a class="toc" id="toc-see-and-create-channels" href="#toc-see-and-create-channels"></a>

You can of course create and delete channels via the dedicated menu in your settings, in the `My Library` > `My Videos Channels` tab.

![Menu presenting options to create a channel](/assets/video-channel_create.png){:class="img-fluid"}

## Notify your audience when a video is published <a class="toc" id="toc-notify-your-audience-when-a-video-is-published" href="#toc-notify-your-audience-when-a-video-is-published"></a>

Your friends and your broader audience can see and get notified of new videos without having to check them out regularly. To achieve that, they can "follow" one of your channels (or your whole profile):

* via their PeerTube account
* via their Fediverse account (supposes they have an account on a federated platform like Mastodon or Pleroma)
* via a syndication format (supposes they have an RSS or Atom aggregator)

![Popover presenting options to follow a channel via syndication feeds](/assets/video-channel_rss.png){:class="img-fluid"}
