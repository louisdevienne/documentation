#!/bin/bash

set -au

git clone https://github.com/Chocobozzz/PeerTube /tmp/peertube
pushd /tmp/peertube
first_appearance_of_spec=$( git log --format=%H support/doc/api/openapi.yaml | tail -1 )
array_t=$( git tag --contains "$first_appearance_of_spec" )
popd
rm -rf /tmp/peertube

node scripts/getReleases.js "${array_t[@]}" > js/src/releases.json
