#!/bin/bash
# Sets the PeerTube version number in _config.yml
set -ex

if [ -z "$PEERTUBE_VERSION" -o -z "$PEERTUBE_RC" ]; then
  echo 'Please provide PEERTUBE_VERSION and PEERTUBE_RC environment variables.'
  echo 'PEERTUBE_RC should be "true" to release an RC build.'
  exit 1
fi

configFile=`dirname $0`/../_config.yml

if [ "$PEERTUBE_RC" = "true" ]; then
  sed -i -e "s/latest_rc_version:.\+/latest_rc_version: $PEERTUBE_VERSION/" "$configFile"
  sed -i -e 's/show_rc:.\+/show_rc: true/' "$configFile"
else
  sed -i -e "s/latest_version:.\+/latest_version: $PEERTUBE_VERSION/" "$configFile"

  # If the stable version is newer than the latest RC, we should hide the RC
  latestRCVersion=`grep -oP 'latest_rc_version: \K([0-9\.]+)' "$configFile"`
  ! dpkg --compare-versions $latestRCVersion le $PEERTUBE_VERSION
  if [ $? -ne 0 ]; then
    sed -i -e 's/show_rc:.\+/show_rc: false/' "$configFile"
  fi;
fi
