const toSemver = require('to-semver');

const latest = toSemver(process.argv[2].split('\n'), {
  includePrereleases: false,
  clean: false,
})[0];

const tags = toSemver(process.argv[2].split('\n'), {
  includePrereleases: false,
  clean: false,
}).map(version => {
  return {
    value: version,
    label: version === latest ? version + ' (latest)' : version,
  };
});

console.log(JSON.stringify(tags, null, 2));
